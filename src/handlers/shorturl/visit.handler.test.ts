import request from 'supertest';
import { DI } from '../../index';
import { User } from '../../entities';
import { UserFactory } from '../../database/factories/user.factory';
import { generateHeaders } from '../../tests/jest.helper';
import * as UrlHelper from '../../url.helper';
import { Url } from '../../entities/url.entity';
import { UrlFactory } from '../../database/factories/url.factory';

describe('POST /api/shorturl/:shortUrl : visit short url', () => {
  let user: User;
  let token: string;
  let url: Url;
  let res: Record<string, any>;
  let spiedIsValidShortUrl: jest.SpyInstance;

  beforeAll(async () => {
    await DI.orm.getSchemaGenerator().clearDatabase();

    spiedIsValidShortUrl = jest.spyOn(UrlHelper, 'isValidShortUrl').mockReturnValue(true);

    user = new UserFactory(DI.em).makeOne();
    token = generateHeaders(user);

    url = new UrlFactory(DI.em).makeOne({ user, nbClicks: 0 });

    await DI.em.flush();
    res = await request(DI.app).get(`/api/shorturl/${url.shortUrl}`).set('authorization', token);
  });

  it('should validate url from request body', () => {
    expect(spiedIsValidShortUrl).toHaveBeenCalledWith(url.shortUrl);
  });

  it('should increment the clicks counter for the short url', async () => {
    await DI.em.refresh(url, { disableIdentityMap: true });
    expect(url.nbClicks).toBe(1);
  });

  it('should set response to a valid json', () => {
    expect(res.statusCode).toBe(302);
    expect(res.headers.location).toBe(url.originalUrl);
  });

  describe("when url isn't valid", () => {
    it('should set response to return a specific json error', async () => {
      spiedIsValidShortUrl.mockReturnValueOnce(false);

      const res = await request(DI.app).get(`/api/shorturl/${url.shortUrl}`).set('authorization', token);

      expect(res.statusCode).toBe(400);
      expect(res.body).toEqual({ error: 'Invalid short URL' });
    });
  });

  describe("when url doesn't exist", () => {
    it('should set response to return a specific json error', async () => {
      const res = await request(DI.app).get(`/api/shorturl/nohere`).set('authorization', token);

      expect(res.statusCode).toBe(404);
      expect(res.body).toEqual({ error: 'No route found' });
    });
  });
});
