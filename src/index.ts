import express, { Application } from 'express';
import http from 'http';
import dotenv from 'dotenv';
import { EntityManager, MikroORM, RequestContext } from '@mikro-orm/core';
import type { PostgreSqlDriver } from '@mikro-orm/postgresql';
import { auth } from './middlewares/auth';
import { createShortUrl } from './handlers/shorturl/create.handler';
import { visitShortUrl } from './handlers/shorturl/visit.handler';
import { analyticsShortUrl } from './handlers/shorturl/analytics.handler';

dotenv.config();

export const DI = {} as {
  server: http.Server;
  orm: MikroORM;
  em: EntityManager;
  app: Application;
};

const app: Application = express();
const port = process.env.PORT || 8000;

export const init = (async () => {
  DI.orm = await MikroORM.init<PostgreSqlDriver>();
  DI.em = DI.orm.em;

  app.use(express.json());
  app.use(express.urlencoded());
  app.use((req, res, next) => RequestContext.create(DI.orm.em, next));

  app.post('/api/shorturl', auth, createShortUrl);
  app.get('/api/shorturl/analytics', auth, analyticsShortUrl);
  app.get('/api/shorturl/:short_url', visitShortUrl);

  app.use((req, res) => res.status(404).json({ message: 'No route found' }));

  if (process.env.NODE_ENV !== 'test') {
    DI.server = app.listen(port, () => {
      console.log(`Server is Fire at http://localhost:${port}`);
    });
  }
  DI.app = app;
})();
