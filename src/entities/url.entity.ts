import { Entity, Property, ManyToOne, PrimaryKey, OptionalProps, Unique } from '@mikro-orm/core';
import { nanoid } from 'nanoid';

import { User } from '.';

@Entity()
@Unique({ properties: ['user', 'originalUrl'] })
export class Url {
  [OptionalProps]?: 'nbClicks' | 'createdAt' | 'updatedAt';

  @PrimaryKey()
  id: string = nanoid();

  @Property()
  originalUrl!: string;

  @Property()
  @Unique()
  shortUrl!: string;

  @Property({ default: 0 })
  nbClicks: number = 0;

  @Property()
  createdAt = new Date();

  @Property({ onUpdate: () => new Date() })
  updatedAt = new Date();

  @ManyToOne(() => User)
  user: User;

  constructor(user: User) {
    this.user = user;
  }
}
