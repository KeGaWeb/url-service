import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from 'express';
import { auth } from './auth';
import { DI } from '..';
import { UserFactory } from '../database/factories/user.factory';
import { User } from '../entities';

describe('Auth middleware', () => {
  let user: User;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  const nextFunction: NextFunction = jest.fn();
  const expectedResponse = {
    error: 'Please authenticate',
  };

  beforeEach(() => {
    mockRequest = {};
    mockResponse = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };
  });

  beforeAll(async () => {
    await DI.orm.getSchemaGenerator().clearDatabase();

    user = await new UserFactory(DI.em).createOne();
  });

  describe('when authorization header is missing', () => {
    it('should set a 401 response', () => {
      auth(mockRequest as Request, mockResponse as Response, nextFunction);

      expect(mockResponse.status).toHaveBeenCalledWith(401);
      expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
    });
  });

  describe("when jwt token can't be verified", () => {
    it('should set a 401 response', () => {
      const token = jwt.sign({ sub: user.id }, 'wrong-secret', {
        expiresIn: '10 minutes',
      });
      mockRequest = {
        headers: {
          authorization: `Bearer ${token}`,
        },
      };

      auth(mockRequest as Request, mockResponse as Response, nextFunction);

      expect(mockResponse.status).toHaveBeenCalledWith(401);
      expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
    });
  });

  describe("when decoded jwt token has no 'sub' property", () => {
    it('should set a 401 response', () => {
      const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET!, {
        expiresIn: '10 minutes',
      });
      mockRequest = {
        headers: {
          authorization: `Bearer ${token}`,
        },
      };

      auth(mockRequest as Request, mockResponse as Response, nextFunction);

      expect(mockResponse.status).toHaveBeenCalledWith(401);
      expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
    });
  });

  describe("when decoded jwt token 'sub' property isn't a valid user id", () => {
    it('should set a 401 response', () => {
      const token = jwt.sign({ id: 'not-a-valid-user' }, process.env.JWT_SECRET!, {
        expiresIn: '10 minutes',
      });
      mockRequest = {
        headers: {
          authorization: `Bearer ${token}`,
        },
      };

      auth(mockRequest as Request, mockResponse as Response, nextFunction);

      expect(mockResponse.status).toHaveBeenCalledWith(401);
      expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
    });
  });
});
