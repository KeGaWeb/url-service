import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { User } from '../entities';
import { DI } from '../index';

export interface AuthenticatedRequest extends Request {
  userId: string;
}

export const auth = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.header('Authorization')?.replace('Bearer ', '');
    if (!token) {
      throw new Error();
    }

    const decoded = jwt.verify(token, process.env.JWT_SECRET!);
    if (!decoded.sub) {
      throw new Error();
    }

    const userId = String(decoded.sub);
    // User must exist
    await DI.em.findOneOrFail(User, { id: userId });
    (req as AuthenticatedRequest).userId = String(decoded.sub);

    next();
  } catch (err) {
    res.status(401).send({ error: 'Please authenticate' });
  }
};
