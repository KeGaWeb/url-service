import { Migration } from '@mikro-orm/migrations';

export class Migration20231106233635 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "url" alter column "nb_clicks" type int using ("nb_clicks"::int);');
  }

  async down(): Promise<void> {
    this.addSql('alter table "url" alter column "nb_clicks" type varchar using ("nb_clicks"::varchar);');
  }

}
