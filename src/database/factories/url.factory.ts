import { Factory, Faker } from '@mikro-orm/seeder';
import { nanoid } from 'nanoid';
import { Url } from '../../entities';

export class UrlFactory extends Factory<Url> {
  model = Url;

  definition(faker: Faker): Partial<Url> {
    return {
      id: nanoid(),
      originalUrl: faker.internet.url(),
      shortUrl: nanoid(6),
      nbClicks: Number(faker.random.numeric()),
      createdAt: faker.date.past(),
      updatedAt: faker.date.recent(),
    };
  }
}
