import { DI, init } from '..';

beforeAll(async () => {
  await init;
  await DI.orm.config.getDriver().reconnect();
  await DI.orm.getSchemaGenerator().clearDatabase();
});

afterAll(async () => {
  await DI.orm.close(true);
  DI.server?.close();

  jest.restoreAllMocks();
});
