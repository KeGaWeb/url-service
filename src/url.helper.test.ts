import 'jest';
import { nanoid } from 'nanoid';
import { DI } from '.';
import { UrlFactory } from './database/factories/url.factory';
import { UserFactory } from './database/factories/user.factory';
import { Url, User } from './entities';
import { getShortUrl, isValidShortUrl, isValidUrl } from './url.helper';

describe('Url helper', () => {
  describe('isValidUrl', () => {
    describe.each`
      url                        | expectedValue
      ${null}                    | ${'false'}
      ${'meh'}                   | ${'false'}
      ${'https://lunii'}         | ${'false'}
      ${'www.lunii.com'}         | ${'false'}
      ${'https://www.lunii.com'} | ${'true'}
    `('when value is $url', ({ url, expectedValue }) => {
      it(`should return ${expectedValue}`, () => {
        expect(isValidUrl(url)).toBe(expectedValue === 'true');
      });
    });
  });

  describe('isValidShortUrl', () => {
    describe.each`
      url                | expectedValue
      ${null}            | ${'false'}
      ${'10GP-'}         | ${'false'}
      ${'10GPm-zz'}      | ${'false'}
      ${'https://lunii'} | ${'false'}
      ${'www.lunii.com'} | ${'false'}
      ${'10GPm-'}        | ${'true'}
    `('when value is $url', ({ url, expectedValue }) => {
      it(`should return ${expectedValue}`, () => {
        expect(isValidShortUrl(url)).toBe(expectedValue === 'true');
      });
    });
  });

  describe('getShortUrl', () => {
    let user: User;
    let otherUser: User;
    let userUrl: Url;
    let otherUserUrl: Url;
    const sharedUrl = 'https://www.lunii.com';

    beforeAll(async () => {
      await DI.orm.getSchemaGenerator().clearDatabase();

      const userFactory = new UserFactory(DI.orm.em);
      const urlFactory = new UrlFactory(DI.orm.em);

      [user, otherUser] = userFactory.make(2);
      otherUserUrl = urlFactory.makeOne({ user: otherUser, originalUrl: sharedUrl, shortUrl: nanoid(6) });
      userUrl = urlFactory.makeOne({ user });
      await DI.em.flush();
    });

    it('should generate, save and return the short url', async () => {
      const originalUrl = 'https://www.lunii.com/my-first';
      const createdUrl = await getShortUrl(originalUrl, user.id);
      expect(createdUrl).toMatchObject({
        id: expect.anything(),
        shortUrl: expect.any(String),
        originalUrl: originalUrl,
        nbClicks: 0,
        user: expect.objectContaining({ id: user.id }),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
      });

      const dbUrl = await DI.em.findOne(Url, { originalUrl });
      expect(dbUrl?.id).toBe(createdUrl?.id);
    });

    describe('when another user generated a short url for the same original url', () => {
      it('should generate, save and return a short url', async () => {
        const createdUrl = await getShortUrl(sharedUrl, user.id);
        expect(createdUrl?.originalUrl).toBe(sharedUrl);
        expect(createdUrl?.shortUrl).not.toBe(otherUserUrl.shortUrl);
      });
    });

    describe('when the user already generated a short url for the original url', () => {
      it('should do nothing and return the existing url', async () => {
        const createdUrl = await getShortUrl(userUrl.originalUrl, user.id);
        expect(createdUrl).toMatchObject({ originalUrl: userUrl.originalUrl, shortUrl: userUrl.shortUrl });
      });
    });
  });
});
