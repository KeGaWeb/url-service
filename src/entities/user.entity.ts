import { Cascade, Collection, Entity, OneToMany, Property, PrimaryKey } from '@mikro-orm/core';
import { Url } from '.';

@Entity()
export class User {
  @PrimaryKey()
  id!: string;

  @Property()
  createdAt = new Date();

  @Property({ onUpdate: () => new Date() })
  updatedAt = new Date();

  @OneToMany(() => Url, (url) => url.user, { cascade: [Cascade.ALL] })
  urls = new Collection<Url>(this);
}
