import { Options } from '@mikro-orm/core';
import type { PostgreSqlDriver } from '@mikro-orm/postgresql';

const options: Options<PostgreSqlDriver> = {
  type: 'postgresql',
  dbName: process.env.DB_NAME,
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  entities: ['./dist/entities'],
  entitiesTs: ['./src/entities'],
  migrations: {
    path: './dist/database/migrations',
    pathTs: './src/database/migrations',
    disableForeignKeys: false,
  },
  allowGlobalContext: process.env.NODE_ENV === 'test',
  // debug: true,
};

export default options;
