import { Request, Response } from 'express';
import { Url } from '../../entities';
import { AuthenticatedRequest } from '../../middlewares/auth';
import { getShortUrl, isValidUrl } from '../../url.helper';

/**
 * POST /api/shorturl
 */
export const createShortUrl = async (req: Request, res: Response) => {
  const userId = (req as AuthenticatedRequest).userId;
  const url: string | undefined = req.body?.url;

  if (!isValidUrl(url)) {
    res.status(400).json({ error: 'Invalid URL' });
    return;
  }

  let createdUrl: Url | undefined;
  try {
    createdUrl = await getShortUrl(url!, userId);
    // May happen if there is concurrency over url id, should be handled in a near-future to prevent errors
    if (!createdUrl) {
      throw new Error();
    }
  } catch (err) {
    res.status(500).json({ error: 'Something went wrong, please try again or contact our support team' });
    return;
  }

  res.json({
    originalUrl: createdUrl!.originalUrl,
    shortUrl: createdUrl!.shortUrl,
  });
};
