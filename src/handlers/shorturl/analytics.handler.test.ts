import request from 'supertest';
import { DI } from '../../index';
import { User } from '../../entities';
import { UserFactory } from '../../database/factories/user.factory';
import { generateHeaders } from '../../tests/jest.helper';
import { UrlFactory } from '../../database/factories/url.factory';

describe('POST /api/shorturl/:shortUrl : visit short url', () => {
  let user: User;
  let token: string;

  beforeAll(async () => {
    await DI.orm.getSchemaGenerator().clearDatabase();

    user = await new UserFactory(DI.em).createOne();
    token = generateHeaders(user);
  });

  describe('when current user has no url', () => {
    it('should return an empty array', async () => {
      const res = await request(DI.app).get('/api/shorturl/analytics').set('authorization', token);

      expect(res.statusCode).toBe(200);
      expect(res.body).toEqual([]);
    });
  });

  it("should return an array with all user's url", async () => {
    const urls = await new UrlFactory(DI.em).create(15, { user });

    const res = await request(DI.app).get('/api/shorturl/analytics').set('authorization', token);

    expect(res.statusCode).toBe(200);
    expect(res.body).toEqual(
      urls
        .sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime())
        .map((url) => ({
          originalUrl: url.originalUrl,
          shortUrl: url.shortUrl,
          nbClicks: url.nbClicks,
        })),
    );
    expect(res.body.length).toBe(15);
  });
});
