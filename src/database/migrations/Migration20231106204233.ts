import { Migration } from '@mikro-orm/migrations';

export class Migration20231106204233 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "user" ("id" varchar(255) not null, "created_at" varchar(255) not null, "updated_at" varchar(255) not null, constraint "user_pkey" primary key ("id"));');

    this.addSql('create table "url" ("id" varchar(255) not null, "original_url" varchar(255) not null, "short_url" varchar(255) not null, "nb_clicks" varchar(255) not null default 0, "created_at" varchar(255) not null, "updated_at" varchar(255) not null, "user_id" varchar(255) not null, constraint "url_pkey" primary key ("id"));');
    this.addSql('alter table "url" add constraint "url_short_url_unique" unique ("short_url");');
    this.addSql('alter table "url" add constraint "url_user_id_original_url_unique" unique ("user_id", "original_url");');

    this.addSql('alter table "url" add constraint "url_user_id_foreign" foreign key ("user_id") references "user" ("id") on update cascade;');
  }

}
