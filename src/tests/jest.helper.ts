import jwt from 'jsonwebtoken';
import { User } from '../entities';

export const generateHeaders = (user: User) => {
  const token = jwt.sign({ sub: user.id }, process.env.JWT_SECRET!, {
    expiresIn: '10 minutes',
  });

  return `Bearer ${token}`;
};
