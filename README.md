# url-service

## Getting started

### Dependencies

Make sure to install :

- yarn
- make
- docker

### First installation

In order to install the project, please run :

```
make install
```

### Start app

To run the app locally, use :

```
make start
```

NB : this ensures

- docker services are up
- migrations are done
- the app is launched in watching mode

### Tests

To run tests :

```
make test
```

NB :

- Currently tests are run in band, it should be updated in a near future as tests number grows
- DB is shared between local and test env, it's not perfect and should be separated

### Docker services

Currently there is just one service to handle our database : `postgres`

## API

This project expose 3 endpoints which are listed below.
Note that some endpoints need authentication. To use those endpoints, please be sure to provide a valid header like :

```
{
  Authorization: Bearer <your jwt token>
}
```

Currently, the jwt token is a basic signed jwt using HS256 algorithm (the secret used is defined as an env variable).
Its payload must contain a `sub` parameter, corresponding to a valid user id.

### POST /api/shorturl

**Authentication needed**
This endpoint allows a user to generate a shorter url from a valid one (ex: "http://www.lunii.com" -> "Md3-DN").
The original url must be provided in the request body.

NB: current version generates a 6 characters short url, as you can check [here](https://zelark.github.io/nano-id-cc/) the collision probability could increase quickly. Currently we just throw a 500 error, but it would be a good thing :

- to handle a retry (let's say after 3 attempts)
- think about an alert after X retries, so we can adjust the short url size

### GET /api/shorturl/analytics

**Authentication needed**
This endpoint allows a user to list all of his created short url, with the recorded number of visits for each.

### GET /api/shorturl/:shortUrl

This endpoint allows anybody to "visit" a short url, meaning we redirect them to the original url. We also increment the number of visits.
`shortURl` parameter should match the following regex : `^[A-Za-z0-9_-]{${SHORT_URL_LENGTH}}$`
