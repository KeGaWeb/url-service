import { Request, Response } from 'express';
import { DI } from '../..';
import { Url } from '../../entities';
import { isValidShortUrl } from '../../url.helper';

/**
 * GET /api/shorturl/:shortUrl
 */
export const visitShortUrl = async (req: Request, res: Response) => {
  const shortUrl = req.params?.short_url;

  if (!isValidShortUrl(shortUrl)) {
    res.status(400).json({ error: 'Invalid short URL' });
    return;
  }

  const url = await DI.em.findOne(Url, { shortUrl });
  if (!url) {
    res.status(404).json({ error: 'No route found' });
    return;
  }

  // Increment nbClicks
  url!.nbClicks++;
  await DI.em.flush();

  res.redirect(url!.originalUrl);
};
