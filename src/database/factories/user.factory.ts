import { Factory, Faker } from '@mikro-orm/seeder';
import { nanoid } from 'nanoid';
import { User } from '../../entities';

export class UserFactory extends Factory<User> {
  model = User;

  definition(faker: Faker): Partial<User> {
    return {
      id: nanoid(),
      createdAt: faker.date.past(),
      updatedAt: faker.date.recent(),
    };
  }
}
