# Setup the project by installing and configuring everything required
init:
	yarn install
	cp .env.dist .env
	docker-compose up -d

# Start the project locally
start:
	docker-compose up -d
	npx mikro-orm migration:up
	yarn dev

# Stop the project
stop:
	docker-compose down

# Run tests
test:
	docker-compose up -d
	npx mikro-orm migration:up
	yarn test