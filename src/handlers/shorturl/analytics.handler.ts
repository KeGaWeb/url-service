import { QueryOrder } from '@mikro-orm/core';
import { Request, Response } from 'express';
import { DI } from '../..';
import { Url } from '../../entities';
import { AuthenticatedRequest } from '../../middlewares/auth';

/**
 * GET /api/shorturl/analytics
 */
export const analyticsShortUrl = async (req: Request, res: Response) => {
  const userId = (req as AuthenticatedRequest).userId;

  const urls = await DI.em.find(Url, { user: userId }, { orderBy: { createdAt: QueryOrder.DESC } });

  res.json(urls.map((url) => ({ originalUrl: url.originalUrl, shortUrl: url.shortUrl, nbClicks: url.nbClicks })));
};
