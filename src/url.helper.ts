import * as yup from 'yup';
import { nanoid } from 'nanoid';
import { DI } from '.';
import { Url } from './entities';
import { UniqueConstraintViolationException } from '@mikro-orm/core';

const SHORT_URL_LENGTH = 6;
const DB_UNIQUE_CONSTRAINT_ERROR_CODE = '23505';

export const isValidUrl = (url: string | undefined): boolean => {
  try {
    // parse and assert validity
    const value: string | undefined = yup.string().defined().url().validateSync(url);
    return !!value;
  } catch (err) {
    return false;
  }
};

export const isValidShortUrl = (url: string): boolean => {
  try {
    const regex = new RegExp(`^[A-Za-z0-9_-]{${SHORT_URL_LENGTH}}$`);
    // parse and assert validity
    const value: string | undefined = yup.string().defined().matches(regex).validateSync(url);
    return !!value;
  } catch (err) {
    return false;
  }
};

export const getShortUrl = async (url: string, userId: string): Promise<Url | undefined> => {
  try {
    const shortUrl = nanoid(SHORT_URL_LENGTH);
    const shortUrlEntity = DI.em.create(Url, { shortUrl, originalUrl: url, user: userId });
    await DI.em.persistAndFlush(shortUrlEntity);

    return shortUrlEntity;
  } catch (err) {
    if (err instanceof UniqueConstraintViolationException) {
      if (err.code === DB_UNIQUE_CONSTRAINT_ERROR_CODE) {
        //err?.constraint === 'url_user_id_original_url_unique') {
        return DI.em.findOneOrFail(Url, { originalUrl: url, user: userId });
      }
    }
    throw err;
  }
};
