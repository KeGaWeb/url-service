import request from 'supertest';
import { DI } from '../../index';
import { User } from '../../entities';
import { UserFactory } from '../../database/factories/user.factory';
import { generateHeaders } from '../../tests/jest.helper';
import * as UrlHelper from '../../url.helper';
import { Url } from '../../entities/url.entity';

describe('POST /api/shorturl : create short url', () => {
  let user: User;
  let token: string;
  let res: Record<string, any>;
  let spiedIsValidUrl: jest.SpyInstance;
  let spiedGetShortUrl: jest.SpyInstance;
  const url = 'https://www.lunii.com/';
  const mockedCreatedUrl = { originalUrl: url, shortUrl: 'mehmeh' };

  beforeAll(async () => {
    await DI.orm.getSchemaGenerator().clearDatabase();

    spiedIsValidUrl = jest.spyOn(UrlHelper, 'isValidUrl').mockReturnValue(true);
    spiedGetShortUrl = jest.spyOn(UrlHelper, 'getShortUrl').mockResolvedValue(mockedCreatedUrl as Url);

    user = await new UserFactory(DI.orm.em).createOne();
    token = generateHeaders(user);
    res = await request(DI.app).post('/api/shorturl').set('authorization', token).send({
      url,
    });
  });

  it('should validate url from request body', () => {
    expect(spiedIsValidUrl).toHaveBeenCalledWith(url);
  });

  it('should create the short url, using the helper method', () => {
    expect(spiedGetShortUrl).toHaveBeenCalledWith(url, user.id);
  });

  it('should set response to a valid json', () => {
    expect(res.statusCode).toBe(200);
    expect(res.body).toEqual(mockedCreatedUrl);
  });

  describe("when url isn't valid", () => {
    it('should set response to return a specific json error', async () => {
      spiedIsValidUrl.mockReturnValueOnce(false);

      const res = await request(DI.app).post('/api/shorturl').set('authorization', token).send({
        url,
      });

      expect(res.statusCode).toBe(400);
      expect(res.body).toEqual({ error: 'Invalid URL' });
    });
  });

  describe('when short url creation throws', () => {
    it('should set response to return a specific json error', async () => {
      spiedGetShortUrl.mockRejectedValueOnce({});

      const res = await request(DI.app).post('/api/shorturl').set('authorization', token).send({
        url,
      });

      expect(res.statusCode).toBe(500);
      expect(res.body).toEqual({ error: 'Something went wrong, please try again or contact our support team' });
    });
  });
});
